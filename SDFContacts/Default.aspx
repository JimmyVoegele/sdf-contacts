﻿<%@ Page Title="SDF Contacts" Language="C#" MasterPageFile="~/Site.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SDFContacts._Default" ValidateRequest="false" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function rowSelected(sender, args) {
            $find("<%= ProgramArea.ClientID %>").set_enabled(true);

            currentRowIndex = args.get_gridDataItem().get_element().rowIndex - 1;
            
            var radGrid = $find("<%= RadGrid1.ClientID %>");
            var item = radGrid.get_masterTableView().get_dataItems()[currentRowIndex];

            document.getElementById('<%= this.OwnerName.ClientID %>').innerHTML = radGrid.get_masterTableView().getCellByColumnUniqueName(item, "OwnerName").innerHTML.replace("&nbsp;", "");

            $find("<%= FirstName.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "FirstName").innerHTML.replace("&nbsp;", ""));
            $find("<%= LastName.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "LastName").innerHTML.replace("&nbsp;", ""));
            $find("<%= Suffix.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "Suffix").innerHTML.replace("&nbsp;", ""));

            $find("<%= EmailAddress.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "EmailAddress").innerHTML.replace("&nbsp;", ""));
            $find("<%= OtherEmail.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "OtherEmail").innerHTML.replace("&nbsp;", ""));
            $find("<%= ContactTitle.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "ContactTitle").innerHTML.replace("&nbsp;", ""));
            var ProgramArea = radGrid.get_masterTableView().getCellByColumnUniqueName(item, "ProgramArea").innerHTML.replace("&nbsp;", "");
            $find("<%= ProgramArea.ClientID %>").findItemByText(ProgramArea).select();
            $find("<%= Role.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "Role").innerHTML.replace("&nbsp;", ""));
            $find("<%= SecondaryRole.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "SecondaryRole").innerHTML.replace("&nbsp;", ""));
            $find("<%= MainPhone.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "MainPhone").innerHTML.replace("&nbsp;", ""));
            $find("<%= MobilePhone.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "MobilePhone").innerHTML.replace("&nbsp;", ""));
            $find("<%= AltPhone.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "AltPhone").innerHTML.replace("&nbsp;", ""));
            $find("<%= FaxPhone.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "FaxPhone").innerHTML.replace("&nbsp;", ""));

            $find("<%= Address.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "Address").innerHTML.replace("&nbsp;", ""));
            $find("<%= City.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "City").innerHTML.replace("&nbsp;", ""));
            $find("<%= State.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "State").innerHTML.replace("&nbsp;", ""));
            $find("<%= Zip.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "Zip").innerHTML.replace("&nbsp;", ""));

            $find("<%= Notes.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "Notes").innerHTML.replace("&nbsp;", ""));
            $find("<%= Organization.ClientID %>").set_value(radGrid.get_masterTableView().getCellByColumnUniqueName(item, "Organization").innerHTML.replace("&nbsp;", ""));

            var emailaddress = "<html><span><a href=mailto:" + radGrid.get_masterTableView().getCellByColumnUniqueName(item, "EmailAddress").innerHTML.replace("&nbsp;", "") + ">Email:</a></span></html>";
            document.getElementById('<%=EmailLink.ClientID%>').innerHTML = emailaddress;

            var otheremailaddress = "<html><span><a href=mailto:" + radGrid.get_masterTableView().getCellByColumnUniqueName(item, "OtherEmail").innerHTML.replace("&nbsp;", "") + ">Other Email:</a></span></html>";
            document.getElementById('<%=OtherEmailLink.ClientID%>').innerHTML = otheremailaddress;

            var Active = radGrid.get_masterTableView().getCellByColumnUniqueName(item, "Active").innerHTML.replace("&nbsp;", "");
            if (Active == "True")
                document.getElementById('<%=chkActive1.ClientID%>').checked = true;
            else
                document.getElementById('<%=chkActive1.ClientID%>').checked = false;

            var bEnabled = true;

            if (ProgramArea == "GIFTSOnline" || ProgramArea == "RaisersEdge") {
                bEnabled = false;
            }
            else {
                var OwnerName = document.getElementById('<%=OwnerName.ClientID%>').innerHTML;
                var CurrentUser = document.getElementById('<%=lblCurrentUser.ClientID%>').innerHTML;
                if (!CurrentUser.includes(OwnerName)) {
                    bEnabled = false;
                }
            }

            if (bEnabled)
            {
                var button2 = $find("<%= btnSave.ClientID %>");
                button2.set_enabled(true);
                document.getElementById('<%=FirstName.ClientID%>').readOnly = false;
                document.getElementById('<%=LastName.ClientID%>').readOnly = false;
                document.getElementById('<%=Suffix.ClientID%>').readOnly = false;
                document.getElementById('<%=EmailAddress.ClientID%>').readOnly = false;
                document.getElementById('<%=OtherEmail.ClientID%>').readOnly = false;
                document.getElementById('<%=ContactTitle.ClientID%>').readOnly = false;
                document.getElementById('<%=Organization.ClientID%>').readOnly = false;
                document.getElementById('<%=Role.ClientID%>').readOnly = false;
                document.getElementById('<%=SecondaryRole.ClientID%>').readOnly = false;
                document.getElementById('<%=MainPhone.ClientID%>').readOnly = false;
                document.getElementById('<%=MobilePhone.ClientID%>').readOnly = false;
                document.getElementById('<%=AltPhone.ClientID%>').readOnly = false;
                document.getElementById('<%=FaxPhone.ClientID%>').readOnly = false;
                document.getElementById('<%=Notes.ClientID%>').readOnly = false;
                document.getElementById('<%=Address.ClientID%>').readOnly = false;
                document.getElementById('<%=City.ClientID%>').readOnly = false;
                document.getElementById('<%=State.ClientID%>').readOnly = false;
                document.getElementById('<%=Zip.ClientID%>').readOnly = false;
                $find("<%= ProgramArea.ClientID %>").set_enabled(true);
            }
                
            else {
                var button2 = $find("<%= btnSave.ClientID %>");
                button2.set_enabled(false);
                document.getElementById('<%=FirstName.ClientID%>').readOnly = true;
                document.getElementById('<%=LastName.ClientID%>').readOnly = true;
                document.getElementById('<%=Suffix.ClientID%>').readOnly = true;
                document.getElementById('<%=EmailAddress.ClientID%>').readOnly = true;
                document.getElementById('<%=OtherEmail.ClientID%>').readOnly = true;
                document.getElementById('<%=ContactTitle.ClientID%>').readOnly = true;
                document.getElementById('<%=Organization.ClientID%>').readOnly = true;
                document.getElementById('<%=Role.ClientID%>').readOnly = true;
                document.getElementById('<%=SecondaryRole.ClientID%>').readOnly = true;
                document.getElementById('<%=MainPhone.ClientID%>').readOnly = true;
                document.getElementById('<%=MobilePhone.ClientID%>').readOnly = true;
                document.getElementById('<%=AltPhone.ClientID%>').readOnly = true;
                document.getElementById('<%=FaxPhone.ClientID%>').readOnly = true;
                document.getElementById('<%=Notes.ClientID%>').readOnly = true;
                document.getElementById('<%=Address.ClientID%>').readOnly = true;
                document.getElementById('<%=City.ClientID%>').readOnly = true;
                document.getElementById('<%=State.ClientID%>').readOnly = true;
                document.getElementById('<%=Zip.ClientID%>').readOnly = true;
                $find("<%= ProgramArea.ClientID %>").set_enabled(false);
            }
        }

        function GridCreated(sender, eventArgs)
        {
            var scrollArea = document.getElementById(sender.get_element().id + "_GridData");
            var row = sender.get_masterTableView().get_selectedItems()[0];
            ////if the position of the selected row is below the viewable grid area  
            if (row)
            {
                if ((row.get_element().offsetTop - scrollArea.scrollTop) + row.get_element().offsetHeight + 20 > scrollArea.offsetHeight) {
                        //scroll down to selected row  
                        scrollArea.scrollTop = scrollArea.scrollTop + ((row.get_element().offsetTop - scrollArea.scrollTop) +
                        row.get_element().offsetHeight - scrollArea.offsetHeight) + row.get_element().offsetHeight;
                }
                //if the position of the the selected row is above the viewable grid area  
                else if ((row.get_element().offsetTop - scrollArea.scrollTop) < 0) {
                    //scroll the selected row to the top  
                    scrollArea.scrollTop = row.get_element().offsetTop;
                }
            }
        }
    </script>
    <br />
    <telerik:RadLabel runat="server" ID="lblCurrentUser" Font-Size="X-Large" style="color:#0091ba"></telerik:RadLabel>

    <hr />

    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Office2007">
        <Tabs>
            <telerik:RadTab Text="Current Contact" Width="200px" Selected="true"></telerik:RadTab>
            <telerik:RadTab Text="New Contact" Width="200px"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0" BorderWidth="1">
        <telerik:RadPageView runat="server" ID="RadPageView1">
            <br />
            <table border="0" style="width: 100%;border:hidden;margin-left:15px">
                <tr>
                    <td>First name:</td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="FirstName" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Last name:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="LastName" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Suffix:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="Suffix" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>
                        <telerik:RadLabel runat="server" ID="EmailLink" Text="<html><span><a href=mailto:jimmy@veicorporation.com>Email:</a></span></html>"></telerik:RadLabel>
                    </td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="EmailAddress" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>
                        <telerik:RadLabel runat="server" ID="OtherEmailLink" Text="<html><span><a href=mailto:jimmy@veicorporation.com>Other Email:</a></span></html>"></telerik:RadLabel>
                    </td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="OtherEmail" runat="server" Width="250px"></telerik:RadTextBox>                        
                    </td>
                </tr>
                <tr>
                    <td>Program Area:</td>
                    <td>
                        <telerik:RadCombobox runat="server" ID="ProgramArea" Width="250px">
                        </telerik:RadCombobox>
                    </td>
                    <td>Job Title:</td>
                     <td>
                         <telerik:RadTextBox RenderMode="Lightweight" ID="ContactTitle" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Organization:</td>
                     <td>
                         <telerik:RadTextBox RenderMode="Lightweight" ID="Organization" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Primary Role:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="Role" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Secondary Role:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="SecondaryRole" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>Main Phone:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="MainPhone" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Mobile Phone:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="MobilePhone" runat="server" Width="250px"></telerik:RadTextBox>                    
                    </td>
                    <td>Other Phone:</td>
                     <td>
                         <telerik:RadTextBox RenderMode="Lightweight" ID="AltPhone" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Fax Phone:</td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="FaxPhone" runat="server" Width="250px"></telerik:RadTextBox>
                    </td> 
                    <td>Notes:</td>
                    <td rowspan="2">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="Notes" Width="250px" EmptyMessage="Enter Notes" TextMode="MultiLine" Height="75px" Resize="None"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="Address" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>City:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="City" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>State:</td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="State" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Zip Code:</td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="Zip" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
            </table>
            <br />
            <telerik:RadButton ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save Contact" style="left:15px" Skin="Office2007" />
            <asp:Label runat="server" Text="Owner Name:" style="padding-left:70px" />
            <asp:Label ID="OwnerName" runat="server" style="left:160px"></asp:Label>
            <div style="display:block;float:right;">
                <asp:CheckBox runat="server" ID="chkActive1" Text="Active" style="padding-right:12px"/>
            </div>

            <br />
            <br />
        </telerik:RadPageView>
        <telerik:RadPageView runat="server" ID="RadPageView2">
            <br />
            <table border="0" style="width: 100%;border:hidden;margin-left:15px">
                <tr>
                    <td>First name:</td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="FirstNameNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Last name:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="LastNameNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Suffix:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="SuffixNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>
                        <telerik:RadLabel runat="server" ID="RadLabel1" Text="<html><span><a href=mailto:jimmy@veicorporation.com>Email:</a></span></html>"></telerik:RadLabel>
                    </td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="EmailAddressNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>
                        <telerik:RadLabel runat="server" ID="RadLabel2" Text="<html><span><a href=mailto:jimmy@veicorporation.com>Other Email:</a></span></html>"></telerik:RadLabel>
                    </td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="OtherEmailNew" runat="server" Width="250px"></telerik:RadTextBox>                        
                    </td>
                </tr>
                <tr>
                    <td>Program Area:</td>
                    <td>
                        <telerik:RadCombobox runat="server" ID="ProgramAreaNew" Width="250px">
                        </telerik:RadCombobox>
                    </td>
                    <td>Job Title:</td>
                     <td>
                         <telerik:RadTextBox RenderMode="Lightweight" ID="ContactTitleNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Organization:</td>
                     <td>
                         <telerik:RadTextBox RenderMode="Lightweight" ID="OrganizationNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Primary Role:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="RoleNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Secondary Role:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="SecondaryRoleNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>Main Phone:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="MainPhoneNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Mobile Phone:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="MobilePhoneNew" runat="server" Width="250px"></telerik:RadTextBox>                    
                    </td>
                    <td>Other Phone:</td>
                     <td>
                         <telerik:RadTextBox RenderMode="Lightweight" ID="AltPhoneNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Fax Phone:</td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="FaxPhoneNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td> 
                    <td>Notes:</td>
                    <td rowspan="2">
                        <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="NotesNew" Width="250px" EmptyMessage="Enter Notes" TextMode="MultiLine" Height="75px" Resize="None"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="AddressNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>City:</td>
                    <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="CityNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>State:</td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="StateNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                    <td>Zip Code:</td>
                     <td>
                        <telerik:RadTextBox RenderMode="Lightweight" ID="ZipNew" runat="server" Width="250px"></telerik:RadTextBox>
                    </td>
                </tr>
            </table>
            <br />
            <telerik:RadButton ID="btnNew" runat="server" OnClick="btnNew_Click" Text="Create Contact" style="left:15px" Skin="Office2007"/>
            <br />
            <br />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <br />
<%--    <telerik:RadLabel Text="Search" runat="server"></telerik:RadLabel>
    <telerik:RadSearchBox RenderMode="Lightweight" runat="server" ID="RadSearchBox1" Skin="Office2007"
                DataSourceID="dllDataSource"
                DataKeyNames="ContactID"
                DataTextField="Name"
                DataValueField="ContactID"
                EnableAutoComplete="true"
                ShowSearchButton="false"
                Width="100%"
                OnSearch="RadSearchBox1_Search">
                <DropDownSettings Width="100%" />
            </telerik:RadSearchBox>
     <br />
    <br />--%>
    <telerik:RadButton ID="btnExport" runat="server" OnClick="btnExport_Click" Text="Export Contact" style="left:15px" Skin="Office2007"/>
    <telerik:RadLabel ID="lblContactCount" runat="server" Text  ="103 Contacts" style="padding-left:45px"></telerik:RadLabel>
    <div style="display:block;float:right;">
        <asp:CheckBox runat="server" ID="chkShowInactive" Text="Show Inactive Contacts" style="padding-right:12px" Checked="false" ViewStateMode="Enabled" AutoPostBack="true" Font-Bold="true" OnCheckedChanged="chkShowInactive_CheckedChanged"/>
    </div>
    <div style="display:block;float:right;">
        <asp:CheckBox runat="server" ID="chkShowMyContacts" Text="Show My Contacts" style="padding-right:12px" Checked="false" ViewStateMode="Enabled" AutoPostBack="true" Font-Bold="true" OnCheckedChanged="chkShowMyContacts_CheckedChanged"/>
    </div>
    <br />
    <br />
    <telerik:RadGrid RenderMode="Lightweight" ID="RadGrid1" AllowPaging="True" CssClass="grid" runat="server" GridLines="None" Height="475px" AllowSorting="true" DataSourceID="SqlDataSource1" Skin="Office2007" AllowFilteringByColumn="True" OnItemCommand="RadGrid1_ItemCommand" OnDataBound="RadGrid1_DataBound" OnItemDataBound="RadGrid1_ItemDataBound" OnPreRender="RadGrid1_PreRender" OnPageSizeChanged="RadGrid1_PageSizeChanged">
        <GroupingSettings CaseSensitive="false" />
        <MasterTableView TableLayout="Fixed" PageSize="200" ClientDataKeyNames="ContactID" AutoGenerateColumns="false" Font-Size="Smaller" CommandItemDisplay="Top">
            <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" AllowFiltering="true" FilterControlWidth="90%"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" AllowFiltering="true" FilterControlWidth="90%"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Suffix" HeaderText="Suffix" AllowFiltering="false" HeaderStyle-Width="0px" FilterControlWidth="90%">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MainPhone" HeaderText="Main Phone" AllowFiltering="true" FilterControlWidth="90%"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MobilePhone" HeaderText="Mobile Phone" AllowFiltering="true" FilterControlWidth="90%"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AltPhone" HeaderText="Alt Phone" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FaxPhone" HeaderText="Fax Phone" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EmailAddress" HeaderText="Email Address" AllowFiltering="true" FilterControlWidth="90%"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="OtherEmail" HeaderText="Other Email" AllowFiltering="true" HeaderStyle-Width="0px" >
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Role" HeaderText="Role"  HeaderStyle-Width="0px" AllowFiltering="false">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SecondaryRole" HeaderText="Secondary Role"  HeaderStyle-Width="0px" AllowFiltering="false">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Organization" HeaderText="Organization" AllowFiltering="true" FilterControlWidth="90%"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ContactTitle" HeaderText="Job Title" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Address" HeaderText="Address"  HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="City" HeaderText="City" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="State" HeaderText="State" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Zip" HeaderText="Zip" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ProgramArea" HeaderText="Program Area" HeaderButtonType="TextButton"  FilterControlWidth="90%">
                    <FilterTemplate>
                        <telerik:RadComboBox RenderMode="Lightweight" ID="ProgramAreaName" DataSourceID="SqlDataSource3" DataTextField="ProgramArea"
                            DataValueField="ProgramArea" Width="215px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("ProgramArea").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="ProgramAreaIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="All" />
                                </Items>
                            </telerik:RadComboBox>
                            <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
                                <script type="text/javascript">
                                    function ProgramAreaIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("ProgramArea", args.get_item().get_value(), "EqualTo");
                                }
                                </script>
                            </telerik:RadScriptBlock>
                    </FilterTemplate>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Notes" HeaderText="Notes"  HeaderStyle-Width="0px">
                    <ItemStyle Font-Size="XX-Small" Wrap="false" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="OwnerName" HeaderText="Owner Name">
                      <FilterTemplate>
                            <telerik:RadComboBox RenderMode="Lightweight" ID="RadOwnerName" DataSourceID="SqlDataSource2" DataTextField="OwnerName"
                                DataValueField="OwnerName" Width="215px" AppendDataBoundItems="true" SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("OwnerName").CurrentFilterValue %>'
                                runat="server" OnClientSelectedIndexChanged="OwnerNameIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Text="All" />
                                </Items>
                            </telerik:RadComboBox>
                            <telerik:RadScriptBlock ID="RadScriptBlock3" runat="server">
                                <script type="text/javascript">
                                    function OwnerNameIndexChanged(sender, args) {
                                        var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("OwnerName", args.get_item().get_value(), "EqualTo");
                                }
                                </script>
                            </telerik:RadScriptBlock>
                        </FilterTemplate>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SDFRole1" HeaderText="SDFRole1" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SDFRole2" HeaderText="SDFRole2" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SDFRole3" HeaderText="SDFRole3" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SDFRole4" HeaderText="SDFRole4" HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SDFRole5" HeaderText="SDFRole5" HeaderStyle-Width="0px">
                    <ItemStyle Font-Size="XX-Small"  Wrap="false" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ContactID" HeaderText="ContactID"  HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="UserID" HeaderText="UserID"  HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Active" HeaderText="Active"  HeaderStyle-Width="0px">
                    <ItemStyle Wrap="false"/>
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings>
            <Selecting AllowRowSelect="true"></Selecting>
            <ClientEvents OnRowSelected="rowSelected"  OnGridCreated="GridCreated"></ClientEvents>
            <Scrolling AllowScroll="true" SaveScrollPosition="false" UseStaticHeaders="true"></Scrolling>
        </ClientSettings>
    </telerik:RadGrid>
    <asp:SqlDataSource ID="dllDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SELECT ContactID, FirstName + ' ' + LastName + ' ' + EmailAddress + ' ' + Address + ' ' + City + ' ' + State + ' ' + Zip + ' ' + MainPhone as Name FROM [dbo].[vwContacts]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select ContactID, ProgramArea, FirstName, LastName, Suffix, EmailAddress, OtherEmail, ContactTitle, Organization, Role, SecondaryRole, MainPhone, MobilePhone, AltPhone, FaxPhone, Address, City, State, Zip, Notes, OwnerName, UserID, SDFRole1, SDFRole2, SDFRole3, SDFRole4, SDFRole5, Active from [dbo].[vwContacts] where Active= 1 and ProgramArea <> @RaisersEdge and OwnerName like @OwnerName order by contactid">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select Distinct OwnerName from [dbo].[vwContacts]">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select Distinct ProgramArea from [dbo].[vwContacts] where ProgramArea <> ''">
    </asp:SqlDataSource>
</asp:Content>