﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

using System.IO;

using Outlook = Microsoft.Office.Interop.Outlook;

using Telerik.Web.UI;
using Microsoft.Win32;

using System.Net;

namespace SDFContacts
{
    public partial class _Default : Page
    {
        private int m_nSelectedContactID = 0;
        static string responseString = string.Empty;
        static bool m_bShowMyContacts = true;

        private int m_nCurrentUSERID = 0;
        static string m_strCurrentUserName = string.Empty;

        private bool m_bShowRaisersEdge = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["RasiersEdge"] = "Jimmy";

            if (!IsPostBack)
            {

                GetCurrentUserID();
                LoadProgramAreaCombos();


                m_bShowMyContacts = ShowMyContacts();
                if (m_bShowMyContacts)
                {
                    RadGrid1.MasterTableView.Columns[19].HeaderStyle.Width = 0;
                    chkShowMyContacts.Checked = true;
                }
                else
                {
                    RadGrid1.MasterTableView.Columns[19].HeaderStyle.Width = 225;
                    chkShowMyContacts.Checked = false;
                }

                try
                {
                    if (!string.IsNullOrEmpty(SqlDataSource1.SelectParameters["RaisersEdge"].DefaultValue) || SqlDataSource1.SelectParameters["RaisersEdge"].DefaultValue == "")
                    {
                        Parameter p = SqlDataSource1.SelectParameters["RaisersEdge"];
                        SqlDataSource1.SelectParameters.Remove(p);
                    }
                }
                catch
                {

                }


                if (m_bShowRaisersEdge)
                    SqlDataSource1.SelectParameters.Add("RaisersEdge", "''");

                else
                    SqlDataSource1.SelectParameters.Add("RaisersEdge", "RaisersEdge");

                try
                {
                    if (!string.IsNullOrEmpty(SqlDataSource1.SelectParameters["OwnerName"].DefaultValue))
                    {
                        Parameter p = SqlDataSource1.SelectParameters["OwnerName"];
                        SqlDataSource1.SelectParameters.Remove(p);
                    }
                }
                catch
                {

                }

                if (m_bShowMyContacts)
                {
                    SqlDataSource1.SelectParameters.Add("OwnerName", m_strCurrentUserName);
                }
                else
                {
                    SqlDataSource1.SelectParameters.Add("OwnerName", "%");
                }
            }
            else
            {
                //string strContactID = ContactID.Text;

                if (m_nSelectedContactID > 0)
                {
                    Console.Write("Find Contact in grid and select it.");
                }
            }
        }

        private void GetCurrentUserID()
        {
            string strLoggedInUserID = HttpContext.Current.User.Identity.Name;
            //if (string.IsNullOrEmpty(strLoggedInUserID))
            //strLoggedInUserID = "eburgess@stdavidsfoundation.org";
            //if (string.IsNullOrEmpty(strLoggedInUserID))
            //    strLoggedInUserID = "jimmy.voegele@gmail.com";

            //strLoggedInUserID = "eburgess@stdavidsfoundation.org";


            //strLoggedInUserID = "kklinkers@stdavidsfoundation.org";
            //if (strLoggedInUserID.ToLower().Contains("klinkers"))
            //    m_bShowRaisersEdge = true;
            //else
                m_bShowRaisersEdge = false;


            lblCurrentUser.Text = Environment.UserName;


            string selectSQL = "Select OwnerID, FirstName, LastName from vwOwnerInfo where userid = '" + strLoggedInUserID + "'";

            Session["USERID"] = strLoggedInUserID;

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strUserID = string.Empty;
            string strUserFirstName = string.Empty;
            string strUserLastName = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strUserID = reader["OwnerID"].ToString();
                    strUserFirstName = reader["FirstName"].ToString();
                    strUserLastName = reader["LastName"].ToString();

                    m_nCurrentUSERID = Convert.ToInt16(strUserID);
                    m_strCurrentUserName = strUserFirstName + " " + strUserLastName;
                    lblCurrentUser.Text = "SDF Contacts (" + m_strCurrentUserName + ")";
                }
                reader.Close();
            }
            catch (Exception err)
            {
                m_nCurrentUSERID = 0;
                if (!strLoggedInUserID.ToLower().Contains("stdavidsfoundation.org"))
                {
                    m_strCurrentUserName = "";
                }
                else
                {
                    m_strCurrentUserName = strLoggedInUserID;
                }
                lblCurrentUser.Text = "Error: " + err.Message;

                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            if (strLoggedInUserID.ToLower().Contains("stdavidsfoundation.org"))
            {
                lblCurrentUser.Text = "SDF Contacts (" + m_strCurrentUserName + ")";
                if (string.IsNullOrEmpty(m_strCurrentUserName))
                    m_strCurrentUserName = strLoggedInUserID;
            }
            else if (string.IsNullOrEmpty(m_strCurrentUserName))
            {
                Response.Redirect("~\\About.aspx");
            }
        }

        private void CreateOutlookContact()
        {
            GridDataItem[] itm = RadGrid1.MasterTableView.GetSelectedItems();

            //OtherEmail.Text = itm[0]["OtherEmail"].Text.Replace("&nbsp;", "");
            //ContactTitle.Text = itm[0]["ContactTitle"].Text.Replace("&nbsp;", "");
            //Suffix.Text = itm[0]["Suffix"].Text.Replace("&nbsp;", "");
            //FaxPhone.Text = itm[0]["FaxPhone"].Text.Replace("&nbsp;", "");
            //Notes.Text = itm[0]["Notes"].Text.Replace("&nbsp;", "");
            if (itm.Length != 0)
            {
                MemoryStream ms = new MemoryStream();
                TextWriter tw = new StreamWriter(ms);

                tw.WriteLine("BEGIN:VCARD");
                tw.WriteLine("VERSION:3.0" + System.Environment.NewLine);
                tw.WriteLine("FN:" + itm[0]["FirstName"].Text.Replace("&nbsp;", "") + " " + itm[0]["LastName"].Text.Replace("&nbsp;", ""));
                tw.WriteLine("ORG:" + itm[0]["Organization"].Text.Replace("&nbsp;", ""));
                tw.WriteLine("TITLE:" + itm[0]["ContactTitle"].Text.Replace("&nbsp;", ""));
                tw.WriteLine("EMAIL:" + itm[0]["EmailAddress"].Text.Replace("&nbsp;", ""));
                tw.WriteLine("TEL;WORK;VOICE:" + itm[0]["MainPhone"].Text.Replace("&nbsp;", ""));
                tw.WriteLine("TEL;MOBILE;VOICE:" + itm[0]["MobilePhone"].Text.Replace("&nbsp;", ""));
                tw.WriteLine("TEL;HOME;VOICE:" + itm[0]["AltPhone"].Text.Replace("&nbsp;", ""));
                tw.WriteLine("ADR;TYPE=BUSINESS:;;" + itm[0]["Address"].Text.Replace("&nbsp;", "") + ";" + itm[0]["City"].Text.Replace("&nbsp;", "") + ";" + itm[0]["State"].Text.Replace("&nbsp;", "") + ";" + itm[0]["Zip"].Text.Replace("&nbsp;", ""));
                tw.WriteLine("END:VCARD");

                tw.Flush();
                byte[] bytes = ms.ToArray();
                ms.Close();

                Response.Clear();
                Response.ContentType = "application/force-download";
                string strVCardName = itm[0]["FirstName"].Text.Replace("&nbsp;", "") + " " + itm[0]["LastName"].Text.Replace("&nbsp;", "") + ".vcf";
                Response.AddHeader("content-disposition", "attachment;    filename=" + strVCardName.Replace(",", ""));
                Response.BinaryWrite(bytes);
                Response.End();
            }
        }

        private void LoadProgramAreaCombos()
        {
            string selectSQL = "Select ProgramArea from vwProgramAreas order by ProgramArea";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strProgramArea = string.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                ProgramArea.Items.Clear();
                ProgramAreaNew.Items.Clear();
                RadComboBoxItem rcbItemEmpty = new RadComboBoxItem("", "");
                ProgramArea.Items.Add(rcbItemEmpty);
                ProgramArea.Items.Add("GIFTSOnline");
                if (m_bShowRaisersEdge)
                    ProgramArea.Items.Add("RaisersEdge");
                while (reader.Read())
                {

                    strProgramArea = reader["ProgramArea"].ToString();

                    RadComboBoxItem rcbItem = new RadComboBoxItem(strProgramArea, strProgramArea);

                    ProgramArea.Items.Add(rcbItem);

                    RadComboBoxItem rcbItemNew = new RadComboBoxItem(strProgramArea, strProgramArea);

                    ProgramAreaNew.Items.Add(rcbItemNew);
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
        }

        private void LoadCurrentContact()
        {
            GridDataItem[] itm = RadGrid1.MasterTableView.GetSelectedItems();

            OwnerName.Text = itm[0]["OwnerName"].Text.Replace("&nbsp;", "");
            LastName.Text = itm[0]["LastName"].Text.Replace("&nbsp;", "");
            Suffix.Text = itm[0]["Suffix"].Text.Replace("&nbsp;", "");
            FirstName.Text = itm[0]["FirstName"].Text.Replace("&nbsp;", "");
            EmailAddress.Text = itm[0]["EmailAddress"].Text.Replace("&nbsp;", "");
            OtherEmail.Text = itm[0]["OtherEmail"].Text.Replace("&nbsp;", "");
            Organization.Text = itm[0]["Organization"].Text.Replace("&nbsp;", "");
            ContactTitle.Text = itm[0]["ContactTitle"].Text.Replace("&nbsp;", "");

            Role.Text = itm[0]["Role"].Text.Replace("&nbsp;", "");
            SecondaryRole.Text = itm[0]["SecondaryRole"].Text.Replace("&nbsp;", "");
            MainPhone.Text = itm[0]["MainPhone"].Text.Replace("&nbsp;", "");
            MobilePhone.Text = itm[0]["MobilePhone"].Text.Replace("&nbsp;", "");
            AltPhone.Text = itm[0]["AltPhone"].Text.Replace("&nbsp;", "");
            FaxPhone.Text = itm[0]["FaxPhone"].Text.Replace("&nbsp;", "");
            Address.Text = itm[0]["Address"].Text.Replace("&nbsp;", "");
            City.Text = itm[0]["City"].Text.Replace("&nbsp;", "");
            State.Text = itm[0]["State"].Text.Replace("&nbsp;", "");
            Zip.Text = itm[0]["Zip"].Text.Replace("&nbsp;", "");
            Notes.Text = itm[0]["Notes"].Text.Replace("&nbsp;", "");
            if (string.IsNullOrEmpty(itm[0]["Active"].Text) || itm[0]["Active"].Text == "True")
                chkActive1.Checked = true;
            else
                chkActive1.Checked = false;


            EmailLink.Text = "<html><span><a href=mailto:" + itm[0]["EmailAddress"].Text.Replace("&nbsp;", "") + ">Email:</a></span></html>";
            OtherEmailLink.Text = "<html><span><a href=mailto:" + itm[0]["OtherEmail"].Text.Replace("&nbsp;", "") + ">Other Email:</a></span></html>";

            string strProgramArea = itm[0]["ProgramArea"].Text;
            if (!string.IsNullOrEmpty(strProgramArea.Replace("&nbsp;", "")) && ProgramArea.FindItemByText(strProgramArea) != null)
                ProgramArea.FindItemByText(strProgramArea).Selected = true;
            else if (ProgramArea.FindItemByText("") != null)
                ProgramArea.FindItemByText("").Selected = true;

            bool bEnabled = true;

            if (strProgramArea == "GIFTSOnline" || strProgramArea == "RaisersEdge")
                bEnabled = false;
            else
            {
                if (lblCurrentUser.Text.Contains(OwnerName.Text))
                    bEnabled = true;
                else
                    bEnabled = false;
            }

            FirstName.ReadOnly = !bEnabled;
            LastName.ReadOnly = !bEnabled;
            Suffix.ReadOnly = !bEnabled;
            EmailAddress.ReadOnly = !bEnabled;
            OtherEmail.ReadOnly = !bEnabled;
            ProgramArea.Enabled = bEnabled;
            ContactTitle.ReadOnly = !bEnabled;
            Organization.ReadOnly = !bEnabled;
            Role.ReadOnly = !bEnabled;
            SecondaryRole.ReadOnly = !bEnabled;
            MainPhone.ReadOnly = !bEnabled;
            MobilePhone.ReadOnly = !bEnabled;
            AltPhone.ReadOnly = !bEnabled;
            FaxPhone.ReadOnly = !bEnabled;
            Notes.ReadOnly = !bEnabled;
            Address.ReadOnly = !bEnabled;
            City.ReadOnly = !bEnabled;
            State.ReadOnly = !bEnabled;
            Zip.ReadOnly = !bEnabled;
            btnSave.Enabled = bEnabled;

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {

            GridDataItem[] itm = RadGrid1.MasterTableView.GetSelectedItems();

            string strContactID = itm[0]["ContactID"].Text;

            bool bActiveContact = true;

            bActiveContact = chkActive1.Checked;

            m_nSelectedContactID = Convert.ToInt32(strContactID);
            string strOwnerName = itm[0]["OwnerName"].Text;
            string Lastname1 = LastName.Text;

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            string selectSQL = "Update [db_owner].[Contacts] SET " +
                "FirstName = '" + FirstName.Text.Replace("'", "''") +
                "', Lastname = '" + LastName.Text.Replace("'", "''") +
                "', Suffix = '" + Suffix.Text.Replace("'", "''") +
                "', EmailAddress = '" + EmailAddress.Text.Replace("'", "''") +
                "', OtherEmail = '" + OtherEmail.Text.Replace("'", "''") +
                "', Organization = '" + Organization.Text.Replace("'", "''") +
                "', Title = '" + ContactTitle.Text.Replace("'", "''") +
                "', ProgramArea = '" + ProgramArea.Text.Replace("'", "''") +
                "', Role = '" + Role.Text.Replace("'", "''") +
                "', SecondaryRole = '" + SecondaryRole.Text.Replace("'", "''") +
                "', MainPhone = '" + MainPhone.Text.Replace("'", "''") +
                "', MobilePhone = '" + MobilePhone.Text.Replace("'", "''") +
                "', AltPhone = '" + AltPhone.Text.Replace("'", "''") +
                "', FaxPhone = '" + FaxPhone.Text.Replace("'", "''") +
                "', Address = '" + Address.Text.Replace("'", "''") +
                "', City = '" + City.Text.Replace("'", "''") +
                "', State = '" + State.Text.Replace("'", "''") +
                "', Zip = '" + Zip.Text.Replace("'", "''") +
                "', Notes = '" + Notes.Text.Replace("'", "''") +
                "', Active = '" + chkActive1.Checked.ToString() +
                "' where ContactID = " + strContactID;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);

            try
            {
                conn.Open();
                cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            RadGrid1.Rebind();
            if (bActiveContact)
            {
                int nIndex = Convert.ToInt32(strContactID);
                foreach (GridDataItem item in RadGrid1.MasterTableView.Items)
                {
                    if (item.GetDataKeyValue("ContactID").ToString() == strContactID)
                    {
                        item.Selected = true;
                        OwnerName.Text = strOwnerName;
                        break;
                    }
                }
            }
            else
            {
                RadGrid1.MasterTableView.Items[0].Selected = true;
                LoadCurrentContact();
            }
            //ReloadGrid();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            GetCurrentUserID();

            string OwenerID = m_nCurrentUSERID.ToString();

            string selectSQL = "Insert into [db_owner].[Contacts] (OwnerID, ProgramArea, FirstName, LastName, Suffix, EmailAddress, OtherEmail, " +
                "Title, Organization, Role, SecondaryRole, MainPhone, MobilePhone, AltPhone, FaxPhone, Address, City, State, Zip, Notes, " +
                "SDFRole1ID, SDFRole2ID, SDFRole3ID, SDFRole4ID, SDFRole5ID) VALUES ( " +
                "'" + OwenerID.ToString().Replace("'", "''") +
                "','" + ProgramAreaNew.Text.Replace("'", "''") +
                "','" + FirstNameNew.Text.Replace("'", "''") +
                "','" + LastNameNew.Text.Replace("'", "''") +
                "','" + SuffixNew.Text.Replace("'", "''") +
                "','" + EmailAddressNew.Text.Replace("'", "''") +
                "','" + OtherEmailNew.Text.Replace("'", "''") +
                "','" + ContactTitleNew.Text.Replace("'", "''") +
                "','" + OrganizationNew.Text.Replace("'", "''") +
                "','" + RoleNew.Text.Replace("'", "''") +
                "','" + SecondaryRoleNew.Text.Replace("'", "''") +
                "','" + MainPhoneNew.Text.Replace("'", "''") +
                "','" + MobilePhoneNew.Text.Replace("'", "''") +
                "','" + AltPhoneNew.Text.Replace("'", "''") +
                "','" + FaxPhoneNew.Text.Replace("'", "''") +
                "','" + AddressNew.Text.Replace("'", "''") +
                "','" + CityNew.Text.Replace("'", "''") +
                "','" + StateNew.Text.Replace("'", "''") +
                "','" + ZipNew.Text.Replace("'", "''") +
                "','" + NotesNew.Text.Replace("'", "''") +
                "','','','','','') SELECT SCOPE_IDENTITY()";

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            int nIndex = 0;
            try
            {
                conn.Open();
                var nReturn = cmd.ExecuteScalar();
                nIndex = Convert.ToInt32(nReturn);
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }
            RadGrid1.Rebind();

            foreach (GridDataItem item in RadGrid1.MasterTableView.Items)
            {
                if (item.GetDataKeyValue("ContactID").ToString() == nIndex.ToString())
                {
                    item.Selected = true;
                    break;
                }
            }

            ProgramAreaNew.Text = "";
            FirstNameNew.Text = "";
            LastNameNew.Text = "";
            SuffixNew.Text = "";
            EmailAddressNew.Text = "";
            OtherEmailNew.Text = "";
            ContactTitleNew.Text = "";
            OrganizationNew.Text = "";
            RoleNew.Text = "";
            SecondaryRoleNew.Text = "";
            MainPhoneNew.Text = "";
            MobilePhoneNew.Text = "";
            AltPhoneNew.Text = "";
            FaxPhoneNew.Text = "";
            AddressNew.Text = "";
            CityNew.Text = "";
            StateNew.Text = "";
            ZipNew.Text = "";
            NotesNew.Text = "";

            RadPageView1.Selected = true;
            RadTab rootTabUser = RadTabStrip1.FindTabByText("Current Contact");
            rootTabUser.Selected = true;
            //ReloadGrid();
        }

        //protected void RadSearchBox1_Search(object sender, SearchBoxEventArgs e)
        //{
        //    RadSearchBox searchBox = (RadSearchBox)sender;

        //    string supplierID = string.Empty;

        //    if (e.DataItem != null)
        //    {
        //        supplierID = ((Dictionary<string, object>)e.DataItem)["ContactID"].ToString();
        //    }

        //    foreach (GridDataItem item in RadGrid1.MasterTableView.Items)
        //    {
        //        if (item.GetDataKeyValue("ContactID").ToString() == supplierID)
        //        {
        //            item.Selected = true;
        //            item.EnableViewState = true;
        //            LoadCurrentContact();
        //            break;
        //        }
        //    }
        //    RadSearchBox1.Text = "";
        //}

        protected void Unnamed_PreRender(object sender, EventArgs e)
        {

        }

        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            RadGrid1.ExportSettings.Excel.Format = Telerik.Web.UI.GridExcelExportFormat.Biff;
            RadGrid1.ExportSettings.IgnorePaging = true;
            RadGrid1.ExportSettings.ExportOnlyData = true;
            RadGrid1.ExportSettings.OpenInNewWindow = true;
        }

        protected void RadGrid1_DataBound(object sender, EventArgs e)
        {
            lblContactCount.Text = RadGrid1.MasterTableView.Items.Count + " Contacts";
            if (RadGrid1.MasterTableView.Items.Count > 0)
            {
                RadGrid1.MasterTableView.Items[0].Selected = true;
                LoadCurrentContact();
                string strProgramArea = RadGrid1.MasterTableView.Items[0]["ProgramArea"].Text;
                if (!string.IsNullOrEmpty(strProgramArea.Replace("&nbsp;", "")) && ProgramArea.FindItemByText(strProgramArea) != null)
                {
                    ProgramArea.FindItemByText(strProgramArea).Selected = true;
                    ProgramArea.Text = strProgramArea;
                }
                else if (ProgramArea.FindItemByText("") != null)
                    ProgramArea.FindItemByText("").Selected = true;
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            CreateOutlookContact();
        }

        protected void chkShowInactive_CheckedChanged(object sender, EventArgs e)
        {
            //m_bShowMyContacts = chkShowMyContacts.Checked;
        }

        protected void chkShowMyContacts_CheckedChanged(object sender, EventArgs e)
        {
            SaveShowMyContacts(chkShowMyContacts.Checked);
            m_bShowMyContacts = chkShowMyContacts.Checked;
            try
            {
                if (!string.IsNullOrEmpty(SqlDataSource1.SelectParameters["OwnerName"].DefaultValue))
                {
                    Parameter p = SqlDataSource1.SelectParameters["OwnerName"];
                    SqlDataSource1.SelectParameters.Remove(p);
                }
            }
            catch
            {

            }

            if (m_bShowMyContacts)
                SqlDataSource1.SelectParameters.Add("OwnerName", m_strCurrentUserName);
            else
                SqlDataSource1.SelectParameters.Add("OwnerName", "%");

            if (m_bShowMyContacts)
            {
                RadGrid1.MasterTableView.Columns[19].HeaderStyle.Width = 0;
            }
            else
            {
                RadGrid1.MasterTableView.Columns[19].HeaderStyle.Width = 225;
            }


        }

        //private void ReloadGrid()
        //{
        //if (m_bShowRaisersEdge)
        //{
        //    SqlDataSource1.SelectCommand = "Select ContactID, ProgramArea, FirstName, LastName, Suffix, EmailAddress, OtherEmail, ContactTitle, Organization, Role, SecondaryRole, MainPhone, MobilePhone, AltPhone, FaxPhone, Address, City, State, Zip, Notes, OwnerName, UserID, SDFRole1, SDFRole2, SDFRole3, SDFRole4, SDFRole5, Active from[dbo].[vwContacts_RaisersEdge]";
        //    SqlDataSource2.SelectCommand = "Select Distinct OwnerName from [dbo].[vwContacts_RaisersEdge]";
        //    SqlDataSource3.SelectCommand = "Select Distinct ProgramArea from [dbo].[vwContacts_RaisersEdge] where ProgramArea <> ''";
        //}
        //else
        //{
        //    SqlDataSource1.SelectCommand = "Select ContactID, ProgramArea, FirstName, LastName, Suffix, EmailAddress, OtherEmail, ContactTitle, Organization, Role, SecondaryRole, MainPhone, MobilePhone, AltPhone, FaxPhone, Address, City, State, Zip, Notes, OwnerName, UserID, SDFRole1, SDFRole2, SDFRole3, SDFRole4, SDFRole5, Active from[dbo].[vwContacts]";
        //    SqlDataSource2.SelectCommand = "Select Distinct OwnerName from [dbo].[vwContacts]";
        //    SqlDataSource3.SelectCommand = "Select Distinct ProgramArea from [dbo].[vwContacts] where ProgramArea <> ''";
        //}

        //if (!chkShowInactive.Checked)
        //{
        //    SqlDataSource1.SelectCommand = SqlDataSource1.SelectCommand + " where Active=1 ";
        //    RadGrid1.MasterTableView.Columns[27].HeaderStyle.Width = 0;
        //}
        //else
        //{
        //    RadGrid1.MasterTableView.Columns[27].HeaderStyle.Width = 67;
        //}

        //if (chkShowMyContacts.Checked)
        //{
        //    GetCurrentUserID();
        //    if (SqlDataSource1.SelectCommand.Contains("where"))
        //    {
        //        SqlDataSource1.SelectCommand = SqlDataSource1.SelectCommand + " and OwnerName = '" + m_strCurrentUserName + "'";
        //    }
        //    else
        //    {
        //        SqlDataSource1.SelectCommand = SqlDataSource1.SelectCommand + " where OwnerName = '" + m_strCurrentUserName + "'";
        //    }
        //    RadGrid1.MasterTableView.Columns[19].HeaderStyle.Width = 0;
        //    m_bShowMyContacts = true;
        //}
        //else
        //{
        //    RadGrid1.MasterTableView.Columns[19].HeaderStyle.Width = 225;
        //    m_bShowMyContacts = false;
        //}

        //SqlDataSource1.SelectCommand = SqlDataSource1.SelectCommand + " order by contactid";
        //RadGrid1.Rebind();

        //}

        protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridPagerItem)
            {
                RadComboBox PageSizeCombo = (RadComboBox)e.Item.FindControl("PageSizeComboBox");
                PageSizeCombo.Items.Clear();
                PageSizeCombo.Items.Add(new RadComboBoxItem("25"));
                PageSizeCombo.FindItemByText("25").Attributes.Add("ownerTableViewId", RadGrid1.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("50"));
                PageSizeCombo.FindItemByText("50").Attributes.Add("ownerTableViewId", RadGrid1.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("100"));
                PageSizeCombo.FindItemByText("100").Attributes.Add("ownerTableViewId", RadGrid1.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("200"));
                PageSizeCombo.FindItemByText("200").Attributes.Add("ownerTableViewId", RadGrid1.MasterTableView.ClientID);
                PageSizeCombo.Items.Add(new RadComboBoxItem("500"));
                PageSizeCombo.FindItemByText("500").Attributes.Add("ownerTableViewId", RadGrid1.MasterTableView.ClientID);
                PageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = true;
            }
        }
        //protected void RadGrid1_PreRender(object sender, EventArgs e)
        //{
        //    if (RadGrid1.MasterTableView.FilterExpression != string.Empty)
        //    {
        //        RefreshCombos();
        //    }
        //}

        //protected void RefreshCombos()
        //{
        //    string str = RadGrid1.MasterTableView.FilterExpression.ToString();
        //    str = CleanString(str);
        //    //SqlDataSource2.SelectCommand = SqlDataSource2.SelectCommand + " WHERE " + str;

        //    //RadGrid1.MasterTableView.Rebind();
        //}

        //private string CleanString(string str)
        //{
        //    str = str.Replace("(", "").Replace(")", "").Replace("Convert.ToStringit","").Replace("[","").Replace("]", "").Replace("\"","");
        //    string strFieldname = str.Substring(0,str.IndexOf("=")).Trim();
        //    string strDataName = str.Substring(str.IndexOf("=")+1).Trim();
        //    str = strFieldname + "='" + strDataName + "'";
        //    return str;
        //}

        private bool ShowMyContacts()
        {

            string selectSQL = "SELECT ShowMyContacts from db_owner.ContactOwner where UserID = '" + Session["USERID"] + "'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strShowMyContacts = string.Empty;


            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strShowMyContacts = reader["ShowMyContacts"].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            bool bShowMyContacts = true;
            try
            {
                bShowMyContacts = Convert.ToBoolean(strShowMyContacts);
            }
            catch
            {
                bShowMyContacts = true;
            }
            return bShowMyContacts;
        }

        private void SaveShowMyContacts(bool bShowMyContacts)
        {

            string selectSQL = "Update db_owner.ContactOwner Set ShowMyContacts = '" + bShowMyContacts + "' where UserID = '" + Session["USERID"] + "'";

            string myConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(myConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);

            try
            {
                conn.Open();
                cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

        }

        private int GetPageSize(string strGrid)
        {

            string selectSQL = "SELECT " + strGrid + " from db_owner.ContactOwner where UserID = '" + Session["USERID"] + "'";

            string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);
            SqlDataReader reader;

            string strRows = string.Empty;


            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    strRows = reader[strGrid].ToString();
                }
                reader.Close();
            }
            catch (Exception err)
            {
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading Focus Areas: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

            int nIndex = 200;
            try
            {
                nIndex = Convert.ToInt16(strRows);
            }
            catch
            {
                nIndex = 200;
            }

            return nIndex;
        }

        private void SetPageSize(string strGrid, int PageIndex)
        {

            string selectSQL = "Update db_owner.ContactOwner Set " + strGrid + " = " + PageIndex + " where UserID = '" + Session["USERID"] + "'";

            string myConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection conn = new SqlConnection(myConnectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, conn);

            try
            {
                conn.Open();
                cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                Console.WriteLine("Debug");
                //Response.Redirect("~/Error.aspx?Error=" + "Error Loading CM Questions: " + err.Message);
                Console.Write(err);
            }
            finally
            {
                conn.Close();
            }

        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            RadGrid1.MasterTableView.PageSize = GetPageSize("DefaultGridRows");
            RadGrid1.MasterTableView.Rebind();
        }

        protected void RadGrid1_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            if (RadGrid1.MasterTableView.PageSize != e.NewPageSize)
                SetPageSize("DefaultGridRows", e.NewPageSize);
        }
    }
}