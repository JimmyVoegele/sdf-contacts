﻿<%@ Page Title="Invalid User" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="SDFContacts.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h3>You do not have a valid valid user profile for this application.  Please contact <a href=mailto:KKLINKERS@stdavidsfoundation.org?subject=Question%20about%20SDFContacts&Body=Please%20add%20me%20as%20a%20user%20to%20SDFContacts.>Kevin Klinkers</a> for more information.</h3>
</asp:Content>

